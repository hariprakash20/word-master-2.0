import { validate } from "./api.js";

const maxLength = 5;
const letters = document.querySelectorAll('.letter');

export function isLetter(letter) {
    return /^[a-zA-Z]$/.test(letter);
}

export function addToGrid(letter, word, currentrow){
    if(word.length < maxLength)
    {
        word += letter;
    }
    else
    {
        word = word.slice(0,4) + letter;
    }
    letters[currentrow*maxLength + word.length-1].innerText = letter;

    if(word.length == maxLength)
    {
        validate(word);
    }
    return word;
}

export function evaluate(word, answer)
{
    const result = document.querySelector('.result');
    if (word == answer){
        result.innerHTML ="Congratulations, You have won the game!";
        return true;
    }
    else {
        result.innerHTML = "Try Again";
        return false;
    }
}

export function colorcoding(word, answer, currentrow){
    let a = word.split("");
    let b = answer.split("");
    for(let i=0;i<5;i++){
        if(a[i]==b[i])
        {
            letters[currentrow*maxLength + i].classList.add('correct-letter');
            b[i]='*';
        }
    }
    for(let i=0;i<5;i++){
        for(let j=0;j<5;j++){
            if(a[i]==b[j])
            {
                letters[currentrow*maxLength + i].classList.add('matching-letter');
                b[j]='*';
                break;
            }
            else
            {
                letters[currentrow*maxLength + i].classList.add('non-matching-letter');
            }
        }
    }
}

export function checkOnlineStatus(){
    const status = document.querySelector('.online-status');
if(navigator.onLine){
    status.innerHTML= "Online";    
}
else{
    status.innerHTML = "Internet connection not available"
}
}

export function invalidWord(currentrow){
    for(let i=0;i<maxLength;i++)
        letters[currentrow*maxLength + i].classList.add('invalid');
    setTimeout(function(){
        for(let i=0;i<maxLength;i++)
        letters[currentrow*maxLength + i].classList.remove('invalid');
    },500);
}

export function deleteLetter(currentrow, word){
    letters[currentrow*maxLength + word.length-1].innerHTML = "";
    return word.substring(0,word.length-1);
}