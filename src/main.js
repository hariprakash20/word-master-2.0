import { getAnswer, validate } from './api.js';
import { isLetter, addToGrid, evaluate, colorcoding, checkOnlineStatus ,invalidWord ,deleteLetter } from './func.js';

const maxLength = 5;

let currentrow = 0;
let letter = '';
let word = "";
let answer ="";
let won = false;
let isValid;

getAnswerApi();

async function getAnswerApi(){
    answer = await getAnswer();
}

if(!won){
    document.addEventListener("keydown",async function (event) {
        let key=event.key;
        if (!isLetter(event.key)) {
            if(key =='Enter' && word.length == maxLength){
                checkOnlineStatus();
                isValid = await validate(word);
                if(isValid){
                    colorcoding(word, answer, currentrow);
                    if(evaluate(word, answer)){
                        won=true;
                    }
                    else{
                        currentrow++;
                        word="";
                    }
                    isValid=false;
                }
                else{   
                    invalidWord(currentrow);
                }
            }
            else if(key == 'Backspace' && word != "")
            {
                word = deleteLetter(currentrow, word);
            }
            else
            event.preventDefault();
        }
        else if(currentrow<6)
        {
            letter = event.key.toUpperCase();
            word = addToGrid(letter, word, currentrow);
        }
    });
}








