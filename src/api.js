export async function getAnswer() {
    const promise = await fetch("https://words.dev-apis.com/word-of-the-day");
    const processedResponse = await promise.json();
    return processedResponse.word.toUpperCase();  
}

export default getAnswer;

export async function validate(word){
    const promise = await fetch('https://words.dev-apis.com/validate-word',{method:'POST', body: JSON.stringify({word: word})});
    const processedResponse = await promise.json();
    return processedResponse.validWord;
}

